import 'package:redux_nav_practice/redux/app_state.dart';
import 'package:redux_nav_practice/redux/reducer/navigation_reducer.dart';

AppState appReducer(AppState state, action) {
  return AppState(
    route: navigationReducer(
      state.route,
      action,
    ),
  );
}
