import 'package:redux/redux.dart';
import 'package:redux_nav_practice/app_routes.dart';
import 'package:redux_nav_practice/redux/app_state.dart';
import 'package:redux_nav_practice/widgets/drawer_selector.dart';

class AppDrawerViewModel {
  AppRoutes route;
  void Function(String route) navigate;
  AppDrawerViewModel({this.navigate, this.route});


  factory AppDrawerViewModel.create(Store<AppState> store) {
    return AppDrawerViewModel(navigate: DrawerSelector.getPushToPage(store));
  }
}