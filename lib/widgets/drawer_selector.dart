import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_nav_practice/redux/app_state.dart';

class DrawerSelector {

  static Function getPushToPage (Store<AppState> store){
    return (String route) => store.dispatch(NavigateToAction.replace(route));
  }

}