import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_nav_practice/app_routes.dart';
import 'package:redux_nav_practice/redux/app_state.dart';
import 'package:redux_nav_practice/widgets/app_drawer_view_model.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState , AppDrawerViewModel >(
      converter: (Store<AppState> store) => AppDrawerViewModel.create(store),
      builder: (ctx, viewModel) => Drawer(
     child: Column(
       crossAxisAlignment: CrossAxisAlignment.start,
       children: <Widget>[
         const SizedBox(
           height: 100.0,
         ),
         FlatButton(
           child: Text('first page'),
           onPressed: () => viewModel.navigate(AppRoutes.firstP),
         ),
         FlatButton(
           child: Text('second page'),
           onPressed: () => viewModel.navigate(AppRoutes.secondP),
         ),
       ],
     ),
   )
    );
  }
}



