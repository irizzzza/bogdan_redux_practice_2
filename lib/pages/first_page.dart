import 'package:flutter/material.dart';
import 'package:redux_nav_practice/widgets/app_drawer.dart';

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Page'),
      ),
      drawer: AppDrawer(),
      backgroundColor: Colors.deepOrange,
    );
  }
}
