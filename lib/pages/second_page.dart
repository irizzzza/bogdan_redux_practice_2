import 'package:flutter/material.dart';
import 'package:redux_nav_practice/widgets/app_drawer.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Page'),
      ),
      drawer: AppDrawer(),
      backgroundColor: Colors.blue,
    );
  }
}
