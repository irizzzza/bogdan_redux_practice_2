
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_nav_practice/app_routes.dart';
import 'package:redux_nav_practice/pages/first_page.dart';
import 'package:redux_nav_practice/pages/second_page.dart';
import 'package:redux_nav_practice/redux/app_state.dart';
import 'package:redux_nav_practice/route_aware_widget.dart';

import 'redux/reducer/app_reducer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final store = Store<AppState>(appReducer,
      initialState: AppState.loading(),
      middleware: [NavigationMiddleware<AppState>()]
  );

  MaterialPageRoute _getRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.firstP:
        return MainRoute(FirstPage(), settings: settings);
      case AppRoutes.secondP:
        return MainRoute(SecondPage(), settings: settings);
      default:
        return MainRoute(FirstPage(), settings: settings);
    }
  }
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        navigatorObservers: [routeObserver],
        onGenerateRoute: (RouteSettings settings) => _getRoute(settings),
        navigatorKey: NavigatorHolder.navigatorKey,
      ),
    );
  }
}


class MainRoute<T> extends MaterialPageRoute<T> {
  MainRoute(Widget widget, {RouteSettings settings})
      : super(
      builder: (_) => RouteAwareWidget(child: widget),
      settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
   // if (settings.isInitialRoute) return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return FadeTransition(opacity: animation, child: child);
  }
}